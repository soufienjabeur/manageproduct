package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metier.CatalogueMetierImp;
import metier.ICatalogueMetier;
import metier.Produit;


public class ControleurServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private ICatalogueMetier metier;
    
    
    public void init() throws ServletException
    {
    	metier=new CatalogueMetierImp();
    }
    public ControleurServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		 ProduitModel model=new ProduitModel();
		 request.setAttribute("mode", model);
         String action=request.getParameter("action");
		 if(action!=null)
		 {
			 if(action.equals("chercher"))
			 {
				 
				 model.setMotCle(request.getParameter("MotCle"));
				 List<Produit> produits=metier.ProduitParMC(model.getMotCle());
				 model.setProduits(produits);
				 			 }
			 else if(action.equals("delete"))
			 {
				 String ref=request.getParameter("ref");
				 metier.deleteProduit(ref);
				 model.setProduits(metier.listProduits());
			 }
			 else if(action.equals("edit"))
			 {
				 String ref=request.getParameter("ref");
				 Produit p= metier.getProduit(ref);
				 model.setPro1(p);
				 model.setMode("edit");
				 model.setProduits(metier.listProduits());
			 }
			 else if(action.equals("save"))
			 {	
				 try{
				 model.getPro1().setReference(request.getParameter("reference"));	
				 model.getPro1().setDesignation(request.getParameter("designation"));
				 model.getPro1().setPrix(Double.parseDouble(request.getParameter("prix")));
				 model.getPro1().setQuantite(Integer.parseInt(request.getParameter("quantite")));
				 if( model.getMode().equals("ajout")) 
				 { metier.addProd(model.getPro1());}
				 else if(model.getMode().equals("edit"))
				 {
					 metier.updateProduit(model.getPro1());
				 }
				 model.setProduits(metier.listProduits());
				 }
				 catch(Exception e)
				 {
					model.setError(e.getMessage()); 
				 }
			 }
		 }
		 
		 
		 request.getRequestDispatcher("VueProduits.jsp").forward(request, response);
	}

}
