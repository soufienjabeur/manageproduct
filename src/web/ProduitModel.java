package web;

import java.util.ArrayList;
import java.util.List;

import metier.Produit;

public class ProduitModel {
	private String motCle;
	private Produit pro1=new Produit();
	private String error;
	private String mode="ajout";
	public String getError() {
		return error;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Produit getPro1() {
		return pro1;
	}
	public void setPro1(Produit pro1) {
		this.pro1 = pro1;
	}
	private List<Produit> produits= new ArrayList<Produit>();
	public String getMotCle() {
		return motCle;
	}
	public void setMotCle(String motCle) {
		this.motCle = motCle;
	}
	public List<Produit> getProduits() {
		return produits;
	}
	public void setProduits(List<Produit> produits) {
		this.produits = produits;
	}

}
