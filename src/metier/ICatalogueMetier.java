package metier;

import java.util.List;

public interface ICatalogueMetier {

	public void addProd(Produit P);
	public List<Produit> listProduits();
	public List<Produit> ProduitParMC(String mc);
	public Produit getProduit(String Ref);
	public void updateProduit(Produit P);
	public void deleteProduit(String Ref);
	
}
