package metier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CatalogueMetierImp implements ICatalogueMetier{

	@Override
	public void addProd(Produit P) {
		
		Connection conn=SingletonConnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement("insert into PRODUITS(REF_PROD,DESIGNATION,PRIX,QUANTITE)values (?,?,?,?)");
			ps.setString(1, P.getReference());
			ps.setString(2, P.getDesignation());
			ps.setDouble(3, P.getPrix());
			ps.setInt(4, P.getQuantite());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	@Override
	public List<Produit> listProduits() {
		List<Produit>prods= new ArrayList<Produit>();
		Connection conn=SingletonConnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement("select* from PRODUITS");
			ResultSet rs=ps.executeQuery();
			while(rs.next())
			{
				Produit p=new Produit();
				p.setReference(rs.getString("REF_PROD"));
				p.setDesignation(rs.getString("DESIGNATION"));
				p.setPrix(rs.getDouble("PRIX"));
				p.setQuantite(rs.getInt("QUANTITE"));
				prods.add(p);
			}
			ps.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prods;
	}

	@Override
	public List<Produit> ProduitParMC(String mc) {
		
		List<Produit>prods= new ArrayList<Produit>();
		Connection conn=SingletonConnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement("select* from PRODUITS WHERE DESIGNATION like ?");
			ps.setString(1,"%"+ mc+"%");
			ResultSet rs=ps.executeQuery();
			while(rs.next())
			{
				Produit p=new Produit();
				p.setReference(rs.getString("REF_PROD"));
				p.setDesignation(rs.getString("DESIGNATION"));
				p.setPrix(rs.getDouble("PRIX"));
				p.setQuantite(rs.getInt("QUANTITE"));
				prods.add(p);
			}
			ps.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prods;
	}

	@Override
	public Produit getProduit(String Ref) {
		Produit p=null;
		Connection conn=SingletonConnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement("select* from PRODUITS WHERE  REF_PROD= ?");
			ps.setString(1,Ref);
			ResultSet rs=ps.executeQuery();
			if(rs.next())
			{
				p=new Produit();
				p.setReference(rs.getString("REF_PROD"));
				p.setDesignation(rs.getString("DESIGNATION"));
				p.setPrix(rs.getDouble("PRIX"));
				p.setQuantite(rs.getInt("QUANTITE"));
				
			}
			ps.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(p==null)throw new RuntimeException("Produit "+ Ref+ " introuvable");
		return p;
	}

	@Override
	public void updateProduit(Produit P) {
		Connection conn=SingletonConnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement("update  PRODUITS set DESIGNATION=?, PRIX=?, QUANTITE=? where REF_PROD=?");
			ps.setString(1, P.getDesignation());
			ps.setDouble(2, P.getPrix());
			ps.setInt(3, P.getQuantite());
			ps.setString(4, P.getReference());
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void deleteProduit(String Ref) {
		
		Connection conn=SingletonConnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement("delete from  PRODUITS where REF_PROD=?");
			ps.setString(1, Ref);
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
